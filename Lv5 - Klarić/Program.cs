﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
namespace Lv5___Klarić
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note("Nota 1", "Tekst 1");
            Note note2 = new Note("Nota 2", "Tekst 2");
            Note note3 = new Note("Nota 3", "Tekst 3");
            Notebook biljeznica = new Notebook();
            biljeznica.AddNote(note1);
            biljeznica.AddNote(note2);
            biljeznica.AddNote(note3);
            IAbstractIterator iterator = biljeznica.GetIterator();
            for (int i = 0; i < biljeznica.Count; i++){
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
