﻿
using System;

namespace ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product1 = new Product("Produkt 1", 2.2);
            Product product2 = new Product("Produkt 2", 3.3);
            Product product3 = new Product("Produkt 3", 4.4);
            Box box = new Box();
            box.AddProduct(product1);
            box.AddProduct(product2);
            box.AddProduct(product3);
            IAbstractIterator iterator = box.GetIterator();
            for (int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
            }
        }
    }
}
