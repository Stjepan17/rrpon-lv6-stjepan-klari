﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD3
{
    class CareTaker
    {
        public List<Memento> list;
        public CareTaker() {
            list = new List<Memento>();
        }
        public void AddMemento(Memento memento) {
            list.Add(memento);
        }
        public void RemoveMemento(Memento memento)
        {   
            list.Remove(memento);    
        }
        public Memento TakeMemento (int index)
        {
            return list[index];
        }
        public Memento PreviousState { get; set; }

    }
}
