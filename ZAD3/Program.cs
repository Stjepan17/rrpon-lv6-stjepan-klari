﻿using System;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
           DateTime a = new DateTime();
           ToDoItem todo = new ToDoItem("A", "t", a);
           Memento memento1 = todo.StoreState();
           todo.Rename("ASd");
           Memento memento2 = todo.StoreState();
           
           CareTaker caretaker = new CareTaker();

           caretaker.AddMemento(memento1);
           caretaker.AddMemento(memento2);
           Console.WriteLine(todo.ToString());
           todo.RestoreState(memento1);
           Console.WriteLine(todo.ToString());
           Console.WriteLine(caretaker.TakeMemento(0).Title);
           Console.WriteLine(caretaker.TakeMemento(1).Title);
           
        }
    }
}