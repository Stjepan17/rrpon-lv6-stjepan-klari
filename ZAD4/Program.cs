﻿using System;

namespace ZAD4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankacc = new BankAccount("Stjepan Klarić", "Adresa 1", 7000);
            Memento memento = bankacc.StoreState();
            bankacc.UpdateBalance(3000);
            Console.WriteLine(bankacc.Balance);
            bankacc.RestoreState(memento);
            Console.WriteLine(bankacc.Balance);
        }
    }
}
